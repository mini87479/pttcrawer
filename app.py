from __future__ import unicode_literals
from random import randint
from flask import Flask, request, abort
import requests
import json
import sys
from urllib.request import urlopen
import configparser
from urllib.request import Request, urlopen
from crawer import pttCrawer

app = Flask(__name__)

ts = []
env = {}

for i in range(1, len(sys.argv)):
    eKey = sys.argv[i].split('=')[0]
    eVal = sys.argv[i].split('=')[1]
    env[eKey] = eVal



results = pttCrawer("Stock")
for result in results:
    print(result["title"])
    ts.append({
        "thumbnailImageUrl": "https://source.unsplash.com/featured/?stock,money?"+str(randint(1, 100)),
        "imageBackgroundColor": "#FFFFFF",
        "title": result["title"],
        "text": result["url"],
        "defaultAction": {
            "type": "uri",
            "label": "查看詳細資料",
            "uri": result["url"]
        },
        "actions": [
            {
                "type": "uri",
                "label": "查看",
                "uri": result["url"]
            }
        ]
    })


url = "https://api.line.me/v2/bot/message/broadcast"

if len(ts) > 10:
    ts = ts[0:10]

prejson = {
    "messages": [
        {
            "type": "template",
            "altText": "每日新聞",
            "template": {
                "type": "carousel",
                "columns": ts,
                "imageAspectRatio": "rectangle",
                "imageSize": "cover"
            }
        }
    ]
}
payload = json.dumps(prejson)
headers = {
    'Authorization': 'Bearer '+ env["Channel_Access_Token"],
    'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)
print(response.status_code)
