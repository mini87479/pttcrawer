from __future__ import unicode_literals
from urllib.request import urlopen
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup


def pttCrawer(subject):
    url = 'https://www.ptt.cc/bbs/{}/index.html'.format(subject)
    header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36'}
    tmp = []
    for i in range(2):
        req = Request(url, headers=header)
        html = urlopen(req)
        bs = BeautifulSoup(html, 'html.parser')
    # title+href
    titles = bs.select('div.title a')
    for title in titles:
        tmp.append({
            "title": title.text,
            "url": "https://www.ptt.cc/{}".format(title['href'])
        })
    return tmp
